﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UploadController : ControllerBase
	{
		private readonly AccDBContext _context;

		public UploadController(AccDBContext context)
		{
			_context = context;
		}

		[HttpPost, DisableRequestSizeLimit]
		public IActionResult PostUpload()
		{
			try
			{
				var file = Request.Form.Files[0];
				var folderName = Path.Combine("Resources", "Uploads");
				var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

				if (file.Length > 0)
				{
					var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
					var fullPath = Path.Combine(pathToSave, fileName);
					var dbPath = Path.Combine(folderName, fileName);

					using (var stream = new FileStream(fullPath, FileMode.Create))
					{
						file.CopyTo(stream);
					}

					return Ok(new { dbPath });
				}
				else
				{
					return BadRequest();
				}
			}
			catch (Exception ex)
			{
				return StatusCode(500, $"Internal server error: {ex}");
			}
		}

		// GET: api/Upload
		[HttpGet]
		public IEnumerable<AccountDetail> GetAccountDetails()
		{
			return _context.AccountDetails;
		}

		// GET: api/Upload/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetAccountDetail([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var accountDetail = await _context.AccountDetails.FindAsync(id);

			if (accountDetail == null)
			{
				return NotFound();
			}

			try
			{
				using (var reader = new StreamReader(accountDetail.FilePath))
				using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
				{
					csv.Configuration.PrepareHeaderForMatch = (string header, int index) => header.ToLower();
					var records = csv.GetRecords<AccountLine>().FirstOrDefault();
					if (records != null)
					{
						return Ok(records);
					}
					else
					{
						return NotFound();
					}
				}
			}
			catch (Exception e)
			{
				return NotFound();
			}
		}
	}
}