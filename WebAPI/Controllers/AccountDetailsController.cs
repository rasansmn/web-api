﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPI.Models;

namespace WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountDetailsController : ControllerBase
	{
		private readonly AccDBContext _context;

		public AccountDetailsController(AccDBContext context)
		{
			_context = context;
		}

		// GET: api/AccountDetails
		[HttpGet]
		public IEnumerable<AccountDetail> GetAccountDetails()
		{
			return _context.AccountDetails;
		}

		// GET: api/AccountDetails/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetAccountDetail([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var accountDetail = await _context.AccountDetails.FindAsync(id);

			if (accountDetail == null)
			{
				return NotFound();
			}

			return Ok(accountDetail);
		}

		// PUT: api/AccountDetails/5
		[HttpPut("{id}")]
		public async Task<IActionResult> PutAccountDetail([FromRoute] int id, [FromBody] AccountDetail accountDetail)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			if (id != accountDetail.AccId)
			{
				return BadRequest();
			}

			_context.Entry(accountDetail).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!AccountDetailExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		// POST: api/AccountDetails
		[HttpPost, DisableRequestSizeLimit]
		public async Task<IActionResult> PostAccountDetail([FromBody] AccountDetail accountDetail)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			_context.AccountDetails.Add(accountDetail);
			await _context.SaveChangesAsync();

			return CreatedAtAction("GetAccountDetail", new { id = accountDetail.AccId }, accountDetail);
		}

		// DELETE: api/AccountDetails/5
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteAccountDetail([FromRoute] int id)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var accountDetail = await _context.AccountDetails.FindAsync(id);
			if (accountDetail == null)
			{
				return NotFound();
			}

			_context.AccountDetails.Remove(accountDetail);
			await _context.SaveChangesAsync();

			return Ok(accountDetail);
		}

		private bool AccountDetailExists(int id)
		{
			return _context.AccountDetails.Any(e => e.AccId == id);
		}
	}
}