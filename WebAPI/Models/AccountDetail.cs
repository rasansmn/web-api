﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
	public class AccountDetail
	{
		[Key]
		public int AccId { get; set; }
		[Required]
		[Column(TypeName = "nvarchar(4)")]
		public string Year { get; set; }
		[Required]
		[Column(TypeName = "varchar(20)")]
		public string Month { get; set; }
		[Required]
		[Column(TypeName = "varchar(100)")]
		public string FilePath { get; set; }
	}
}
