﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.AspNetCore.WebSockets.Internal;

namespace WebAPI.Models
{
	public class AccountLine
	{
		[Name("rnd")]
		public float RND { get; set; }
		[Name("canteen")]
		public float Canteen { get; set; }
		[Name("ceo")]
		public float CEO { get; set; }
		[Name("marketing")]
		public float Marketing { get; set; }
		[Name("parking")]
		public float Parking { get; set; }
	}
}
