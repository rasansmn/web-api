﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Models
{
	public class AccDBContext : DbContext
	{
		public AccDBContext(DbContextOptions<AccDBContext> options) : base(options)
		{
		}

		public DbSet<AccountDetail> AccountDetails { get; set; }
	}
}
